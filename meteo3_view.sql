CREATE OR REPLACE
ALGORITHM = UNDEFINED
VIEW `meteo3_view` AS
SELECT `DateHeure` , `TempIntCabane` , `TempExt`, `Batterie` , `SOC` , `Puissance` , `TempTas1` , `TempTas2` , `TempTas3` , `TempTas4` 
FROM `meteo3`
WHERE 1
ORDER BY `meteo3`.`DateHeure` DESC 
