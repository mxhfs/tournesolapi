CREATE TABLE field_description (
	field_name varchar(255) NOT NULL,
	`translation` varchar(255) NOT NULL,
	unit varchar(255) NULL,
	id INT auto_increment NOT NULL,
	primary key(id)
)
DEFAULT CHARSET=utf8;

