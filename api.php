<?php


if (!function_exists('json_encode')) {
    include_once('JSON.php');
    $GLOBALS['JSON_OBJECT'] = new Services_JSON();

    function json_encode($value) {
        return $GLOBALS['JSON_OBJECT']->encode($value);
    }

    function json_decode($value) {
        return $GLOBALS['JSON_OBJECT']->decode($value);
    }

}

require("global.php");
if (!($link = mysql_connect($hostName, $userName, $password))) {
    printf("<BR> error in connecting to the host %s <BR>\n", $hostName);
    exit();
}
mysql_select_db($dbName);

function getSensorData() {
    //normally this info would be pulled from a database.
    //build JSON array
    $query = "SELECT * FROM meteo3_view order by DateHeure desc limit 1";
    $result = mysql_query($query) or die($query . " - " . mysql_error());
    $app_list = mysql_fetch_array($result, MYSQL_ASSOC);

    return $app_list;
}

function getTranslation() {
    //normally this info would be pulled from a database.
    //build JSON array
    $query = "SELECT * FROM field_description";
    mysql_query("set names 'utf8'");
    $result = mysql_query($query) or die($query . " - " . mysql_error());
    
    $app_list = array();

    while (($row = mysql_fetch_array($result, MYSQL_ASSOC))) {
        $app_list[] = $row;
    }

    return $app_list;
    return $app_list;
}

function getSensorHistory($sensorName, $fromDate, $toDate) {
    //normally this info would be pulled from a database.
    //build JSON array
    $query = "SELECT DateHeure, " . $sensorName . " FROM (SELECT DateHeure, " . $sensorName . " FROM meteo3_view where " . $sensorName . " is not null and DateHeure BETWEEN STR_TO_DATE('" . $fromDate . ":00', '%d-%m-%Y %H:%i:%s') AND STR_TO_DATE('" . $toDate . ":59', '%d-%m-%Y %H:%i:%s') order by DateHeure desc ) sub ORDER BY DateHeure ASC";
    $result = mysql_query($query) or die($query . " - " . mysql_error());
    $app_list = array();

    while (($row = mysql_fetch_array($result, MYSQL_ASSOC))) {
        $app_list[] = $row;
    }

    return $app_list;
}

function getSensorHistoryWithLimit($sensorName, $limit) {
    //normally this info would be pulled from a database.
    //build JSON array
    $query = "SELECT DateHeure, " . $sensorName . " FROM (SELECT DateHeure, " . $sensorName . " FROM meteo3_view where " . $sensorName . " is not null order by DateHeure desc limit " . $limit . ") sub ORDER BY DateHeure ASC";
    $result = mysql_query($query) or die($query . " - " . mysql_error());
    $app_list = array();

    while (($row = mysql_fetch_array($result, MYSQL_ASSOC))) {
        $app_list[] = $row;
    }

    return $app_list;
}


$possible_url = array("getSensorData", "getSensorHistory", "getTranslation");

$value = "An error has occurred";

if (isset($_GET["action"]) && in_array($_GET["action"], $possible_url)) {
    switch ($_GET["action"]) {
        case "getSensorHistory":
            if (isset($_GET["sensorName"])) {
                if (isset($_GET["limit"])) {
                    $limit = $_GET["limit"];
                    $value = getSensorHistoryWithLimit($_GET["sensorName"], $limit);
                } else {
                    $fromDate = $_GET["fromDate"];
                    $toDate = $_GET["toDate"];
                    $value = getSensorHistory($_GET["sensorName"], $fromDate, $toDate);
                }
            } else
                $value = "Missing argument";
            break;
        case "getSensorData":
            $value = getSensorData();
            break;
        case "getTranslation":
            $value = getTranslation();
            break;
    }
}

//return JSON array
exit(json_encode($value));
?>
